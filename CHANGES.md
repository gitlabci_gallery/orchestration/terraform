# 2023-02-28

- !4 Add Windows and Mac OS X runners

# 2023-02-22

- !3 Use cloud-init configuration file instead of shell script

# 2023-02-13

- !1 Unregister gitlab runner on VM destruction
